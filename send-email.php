<?
if (isset($_POST['id']) && !empty($_POST['id'])){
    require_once "library/db/Db.class.php";
    $db = new DB();
    $query = "SELECT * FROM images WHERE id IN ({$_POST['id']})";
    $images = $db->query($query);
    if (!empty($images)){
        $to = "imguploader@admin.com";
        $subject = "Uploaded new pictures.";

        $message = '
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    <title>Uploaded new pictures.</title>
                </head>
                <body>
        ';
        foreach ($images as $img){
            $message .= "<p><a href ='http://" .$_SERVER["SERVER_NAME"] . "/" . $img['path '] ."' target='_blank'>" . $img['name'] . "</a></p>";
        }

        $message .= '</body></html>';

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= "From: imguploader.com\r\n";

        mail($to, $subject, $message, $headers);
    }
}

