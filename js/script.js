window.addEventListener("load", init, false); // The handler to load the document at startup called init function
var counter = makeCounter(); //counter initialization
var output;

function init() {
    var button = document.getElementById("add_file_field");
    var sendFormBtn = document.getElementById("send_form_btn");
    output = document.getElementById("output");

    addEvent(button, "click", handler);
    addEvent(sendFormBtn, "click", sendForm);
 
    loadImageTable();
}

function handler(e) {
    var evt = e || window.event;
    // Discard the standard action of the form submit event
    if (evt.preventDefault) {
        evt.preventDefault(); // for normal browsers
    } else {
        evt.returnValue = false; // for IE older versions
    }

    var form = document.getElementById("form");
    var newFileField = document.createElement('div');

    newFileField.id = "file_" + counter();
    newFileField.innerHTML = "<input type='text' name='fileName' placeholder='File name'/><br />" +
        "<input type='file' name='file' accept='image/*'/>";
    form.appendChild(newFileField);
}
//The function to upload pictures to the servera
function sendForm(e) {
    if (captcha() === false){
        alert("Wrong");
        return false;
    }
    var evt = e || window.event;
    // Discard the standard action of the form submit event
    if (evt.preventDefault) {
        evt.preventDefault(); // for normal browsers
    } else {
        evt.returnValue = false; // for IE older versions
    }
    evt.currentTarget.disabled = true;
    var imageId = [];
    var ajaxResult;
    var validFormItems = [];
    //loop through all elements of the form
    var form = document.getElementById("form");
    for (var i = 0; i < form.children.length; i++) {
        for (var j = 0; j < form.children[i].children.length; j++) {
            if (form.children[i].children[j].type == 'text'){
               var fileName = form.children[i].children[j].value;
            } else if (form.children[i].children[j].type == 'file'){
                var file = form.children[i].children[j].files[0];
            }
        }
        if (file){
            var formData = new FormData();
            formData.append("imageFile", file);
            if (fileName != ''){
                formData.append("fileName", fileName);
            }
            ajaxResult = sendAjaxRequest(formData, form.children[i].id);
            if (ajaxResult){
                loadImageTable();
                var validFormItemId = form.children[i].id;
                validFormItems.push(validFormItemId);
                imageId.push(ajaxResult);
            }
        }
    }

    for (var i=0; i<validFormItems.length; i++){
        document.getElementById(validFormItems[i]).remove();
    }

    //Sending emails containing information about uploaded images
    if (imageId.length > 0){
        ajaxSendEmail(imageId);
    }
    evt.currentTarget.disabled = false;
}

//Function - Sending emails containing information about uploaded images
function ajaxSendEmail(imagesId){
    var url = "send-email.php";
    var xhr = getXhrObject();
    var params = "id=" + imagesId.join(',');

    if (xhr) {
        xhr.open('POST', url, false); // open connection
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.setRequestHeader('Connection', 'close');
        // start ajax request
        xhr.send(params);
    }
}

//Ajax request function
function sendAjaxRequest(formData, formId) {
    var url = "uploading.php";
    var xhr = getXhrObject();
    var imgId;
    if (xhr) {
        xhr.open('POST', url, false); // open connection

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) { // check the processing stage and the status of the server response
                var response = JSON.parse(xhr.responseText);
                if (response.success == false){
                    var formBlock = document.getElementById(formId);
                    var errorBlock = document.getElementById(formId + '_errorMsg');
                    if (!errorBlock){
                        var errorHtml = document.createElement('div');
                        errorHtml.id = formId + '_errorMsg';
                        errorHtml.className = 'inputMsg';
                        errorHtml.innerHTML = response.message;
                        formBlock.appendChild(errorHtml);
                    }else{
                        errorBlock.innerHtml = response.message;
                    }

                    imgId = false;
                }else{
                    imgId = response.img_id;
                }
            }
        }
        //ajax request - start
        xhr.send(formData);
    }
    return imgId;
}
//Ajax function - output tables in the document
function loadImageTable() {
    var tableBlock = document.getElementById('img-list-block');
    tableBlock.innerHTML = "Loading ...";
    var url = "image-table.php";
    var xhr = getXhrObject();
    if (xhr) {
        xhr.open('GET', url); // open connection
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4){
                tableBlock.innerHTML = xhr.responseText;
            }
        }
        xhr.send(null);
    }
}

//Create cross-browser event handlers
function addEvent(elem, type, handler) {
    if (elem.addEventListener) {
        elem.addEventListener(type, handler, false);
    } else {
        elem.attachEvent('on' + type, handler);
    }
    return false;
}
//Universal function to create a new XMLHttpRequest object
function getXhrObject() {
    if (typeof XMLHttpRequest === 'undefined') {
        XMLHttpRequest = function () {
            try {
                return new window.ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
            }
        };
    }
    return new XMLHttpRequest
}

//This function provides a simple captcha
function captcha()
{
    var number1 = getRandomInRange(1, 10);
    var number2 = getRandomInRange(1, 10);
    var sum = number1 + number2;
    var usrSum = prompt(number1 + "+" + number2 + "=?");

    if (sum == usrSum){
        return true;
    }else{
        return false;
    }
}
//Generating an integer in the range
function getRandomInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//Counter to generate unique id
function makeCounter() {
    var currentCount = 1;

    return function () {
        return currentCount++;
    };
}

