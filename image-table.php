<?php
require_once "library/db/Db.class.php";

$db = new DB();
$sql = "SELECT * FROM images";
$images_list = $db->query($sql);

if (!empty($images_list)){
    ?>
        <table class="image-list" align="center">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Preview</th>
                    <th>Size (byte)</th>
                    <th>Link</th>
                </tr>
            </thead>
<?php
    foreach ($images_list as $img){
        ?>
            <tr>
                <td>
                    <?= $img['name'];?>
                </td>
                <td>
                    <img src="<?=$img['preview_path']?>" alt="Preview">
                </td>
                <td>
                    <?=$img['size'];?>
                </td>
                <td>
                    <a href="http:\\<?=$_SERVER["SERVER_NAME"] . '/' . $img['path'];?>" target="_blank">http:\\<?=$_SERVER["SERVER_NAME"] . '/' . $img['path'];?></a>
                </td>

            </tr>    
        <?php

    }
}
?>
</table>
