<?php

require_once "library/db/Db.class.php";
//file download path
$path = 'images';
$tmp_path = 'tmp/';

$types = array('image/gif', 'image/png', 'image/jpeg'); //An array of valid file type

$size = 1024000; //Maximum image size

$db = new DB();
$check_bd = $db->query("show tables like 'images'"); //Check for tables in the database
if (empty($check_bd)) {
    //Creating a table
    $create_table = $db->query("CREATE TABLE `images` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `name` varchar(255) NOT NULL,
                              `base_name` varchar(255) NOT NULL,
                              `server_name` varchar(100) NOT NULL,
                              `w` smallint(6) NOT NULL,
                              `h` smallint(6) NOT NULL,
                              `size` int(11) NOT NULL,
                              `preview_path` varchar(255) NOT NULL,
                              `path` varchar(255) NOT NULL,
                              `preview_server_name` varchar(255) NOT NULL,
                              `extension` char(10) NOT NULL,
                              PRIMARY KEY (`id`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=cp1251;");
}

//Processing request
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once "library/FileHelper.php";

    if (empty($_POST['fileName'])) {
        die(json_encode(
            array(
                'success' => false,
                'message' => 'Not filling the file name.'
            )));
    }

    //Checking the file name uniqueness
    $check_file_name_query = "SELECT id from images WHERE name = :name";
    $result = $db->query($check_file_name_query, array('name' => $_POST['fileName']));
    if (!empty($result)){
        die (json_encode(array('success'=>false, 'message' => 'The file name already exists')));
    }

    //Checking file type
    if (!in_array($_FILES['imageFile']['type'], $types)) {
        die (json_encode(array('success'=>false, 'message'=>'Prohibited  file type.')));
    }

    //Checking the file size
    if ($_FILES['imageFile']['size'] > $size) {
        die(json_encode(array('success'=> false, 'message'=>'Too big file size.')));
    }

    // Get the extension of the uploaded file
    $extension = strtolower(substr(strrchr($_FILES['imageFile']['name'], '.'), 1));
    // Generating a unique file name with this extension
    $filename = FileHelper::getRandomFileName($path, $extension);

    // Create an address for the destination file
    $target = $path . '/' . $filename . '.' . $extension;
    $small_img_target = $path . '/preview_' . $filename . '.' . $extension;
    //Copying images in the directory
    if (@!move_uploaded_file($_FILES['imageFile']['tmp_name'], $target)) {
        die (json_encode(array('success' => false, 'message' => 'Failed to upload the file to the server')));
    } else {
        //Creating a preview image
        FileHelper::resize($target, $small_img_target, 100, 100);
        list($width, $height) = getimagesize($target);
        if ($width > 500 || $height > 500){
            $width = 500;
            $height = 500;
            FileHelper::resize($target, false, $width, $height);
        }
        //Preparing data for the record in the database
        $img_data = array(
            'name' => $_POST['fileName'],
            'base_name' => $_FILES['imageFile']['name'],
            'server_name' => $filename . '.' . $extension,
            'preview_server_name' => 'preview_' . $filename . '.' . $extension,
            'extension' => $extension,
            'preview_path' => $small_img_target,
            'path' => $target,
            'w' => $width,
            'h' => $height,
            'size' => $_FILES['imageFile']['size'],
        );
        $query = "INSERT INTO images (name, base_name, server_name, preview_server_name, extension, preview_path, path, w, h, size)
                  VALUES(:name, :base_name, :server_name, :preview_server_name, :extension, :preview_path,
                        :path, :w, :h, :size)";
        $insert = $db->query($query, $img_data);
        $img_id = $db->lastInsertId();
        die(json_encode(array('success' => true, 'message' => 'The file is uploaded to the server.', 'img_id'=> $img_id)));
    }

}




