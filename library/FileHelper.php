<?php
class FileHelper{
    public static function getRandomFileName($path, $extension='')
    {
        $extension = $extension ? '.' . $extension : '';
        $path = $path ? $path . '/' : '';

        do {
            $name = uniqid();
            $file = $path . $name . $extension;
        } while (file_exists($file));

        return $name;
    }

    public static function resize($image, $file_path = false, $w_o = false, $h_o = false)
    {
        if (($w_o < 0) || ($h_o < 0)) {
            return false;
        }
        list($w_i, $h_i, $type) = getimagesize($image);
        $types = array("", "gif", "jpeg", "png");
        $ext = $types[$type];
        if ($ext) {
            $func = 'imagecreatefrom' . $ext;
            $img_i = $func($image);
        } else {
            return false;
        }

        if (!$h_o) $h_o = $w_o / ($w_i / $h_i);
        if (!$w_o) $w_o = $h_o / ($h_i / $w_i);
        $img_o = imagecreatetruecolor($w_o, $h_o);
        imagecopyresampled($img_o, $img_i, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);
        $func = 'image' . $ext;
        if ($file_path) {
            return $func($img_o, $file_path);
        } else {
            return $func($img_o, $image);
        }
    }
}